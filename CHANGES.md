## v2.0.2
* TAR archive on release page instead of plain .js.

## v2.0.1
* README updated.
* First release triggered.

## v2.0.0
* UMD package prepared
